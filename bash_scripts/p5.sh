# . /opt/ros/iron/setup.zsh

# echo "build [N/y]?"
# read build
# if [ "$build" = "y" ]; then 
#     echo "delete cache [N/y]?"
#     read delete
#     if [ "$delete" = "y" ]; then 
#         rm -rf install build  log
#     fi;
#     colcon build
# fi;

. src/planar_robot_python/bash_scripts/kill_all_ros.sh
. install/setup.zsh

ros2 run planar_robot_python trajectory_generator &
ros2 run planar_robot_python high_level_manager &
ros2 run planar_robot_python simulator &
ros2 run planar_robot_python kinematic_model &
ros2 run planar_robot_python controller &
ros2 run planar_robot_python disturbance &
ros2 run plotjuggler plotjuggler &

sleep 2
echo "\n=============================================================="
echo "[Enter] publish a launch true"
echo "------> Load plotjuggler/p5_layout.xml"
read 
ros2 topic pub  /launch std_msgs/msg/Bool "{data: true}" --once 

sleep 2
echo "\n=============================================================="
echo "[Enter] kill all ROS2 processes"
read 
. src/planar_robot_python/bash_scripts/kill_all_ros.sh
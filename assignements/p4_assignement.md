# Practical 4

Develop a `kinematic_model` node complying with the following characteristics:

- Subscribed to a topic `/joint_states`, receiving messages with type `sensor_msgs/msg/JointState`.

- Publisher in a topic named `/kin_data`, in which the cartesian pose and jacobian matrix are published every time a joint position is received (except from the case described below);

- Has parameters `l1` and `l2`, with default value of `1.0`.

In summary, the expected result when running `src/planar_robot_python/bash_scripts/p4.sh` should be the following:

![](../gif/p4.gif)
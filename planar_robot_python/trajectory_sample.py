import rclpy
from rclpy.node import Node
from std_msgs.msg import Float32
from custom_messages.msg import CartesianState
from .position_interpolation.position_interpolation import PositionInterpolation
import numpy as np

class TrajectorySample(Node):
    def __init__(self):
        super().__init__('trajectory_sample')
        # TODO  
        #       - Initialize node to be publisher and subscriber to the necessary topics
        #       - Initialize main variables
        #       - Create callback function able to compute the position and velocity for a given instant t

        pi                  = np.array([1.0,2.0])
        pf                  = np.array([3.0,4.0])
        self.pos_interp     = PositionInterpolation(pi,pf,2.0)

def main(args=None):
    rclpy.init(args=args)
    trajectory_sample = TrajectorySample()
    rclpy.spin(trajectory_sample)
    trajectory_sample.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()